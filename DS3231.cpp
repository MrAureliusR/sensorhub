/*
 * DS3231.cpp
 *
 *  Created on: Aug. 6, 2023
 *      Author: amr
 */

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "DS3231.hpp"
#include "i2c.hpp"

DS3231::DS3231(i2c *givenBus) {
	this->busAccess = givenBus;

}

void DS3231::getTime() {
	uint8_t *buffer = (uint8_t *)malloc(sizeof(uint8_t) * 10);
	buffer[0] = 0x00;
	busAccess->changeAddress(DS3231_addr);
	busAccess->transact(buffer, 7, WRITEREAD); // read first 7 registers
	this->second = buffer[0];
	this->minute = buffer[1];
	this->hour = buffer[2];
	this->dow = buffer[3];
	this->day = buffer[4];
	this->month = buffer[5];
	this->year = buffer[6];

	free(buffer);
	return;
}

void DS3231::setTime() {
	uint8_t *buffer = (uint8_t *)malloc(sizeof(uint8_t) * 10);
	buffer[0] = 0x00;
	busAccess->changeAddress(DS3231_addr);

	buffer[1] = this->second;
	buffer[2] = this->minute;
	buffer[3] = this->hour;
	buffer[4] = this->dow ;
	buffer[5] = this->day;
	buffer[6] = this->month;
	buffer[7] = this->year;
	busAccess->transact(buffer, 8, WRITE); // read first 7 registers
	free(buffer);
	return;
}
}
