# SensorHub BoosterPack playground
## Example Code by Alexander Rowsell

### Description

This repo descended from code I wrote for the HackadayU course on serial buses, but then I decided to expand it
to play with all the other sensors on the BoosterPack. So that's what this repo (will be)(is)?

### Code Overview

This example code is all written in C++. The I2C and 1-Wire libraries are implemented as classes. Instances of these classes
are then passed to the classes that represent the sensors we communicate with, making the user-side code quite simple.

## License

All code is licensed under the Mozilla Public License, v2.0.