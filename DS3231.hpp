/*
 * DS3231.hpp
 *
 *  Created on: Aug. 6, 2023
 *      Author: amr
 */

#ifndef DS3231_HPP_
#define DS3231_HPP_

#include <stdint.h>
#include "i2c.hpp"

#define DS3231_addr 0x68
class DS3231 {
public:
	DS3231(i2c *givenBus);
	void getTime();
	void setTime();
	uint8_t hour, minute, second;
	uint8_t dow, day, month, year;
private:
	i2c *busAccess;
};

#endif /* DS3231_HPP_ */
