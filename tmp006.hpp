/*
 * tmp006.hpp
 *
 *  Created on: Aug. 15, 2020
 *      Author: amr
 */

#ifndef TMP006_HPP_
#define TMP006_HPP_

#include <i2c.hpp>

#define tmp006Addr 0x41
class tmp006 {
  public:
    tmp006(i2c *givenBus);
    void getTemperature(double *temperature);
  private:
    i2c *busAccess;
    const double a1 = 1.75e-03, a2 = -1.678e-05, Tref = 298.15, b0 = -2.94e-05, b1 = -5.7e-07, b2 = 4.63e-09, c2 = 13.4;
    const double S0 = 6e-14;
    double Vobj, Tdie, Tobj, S, Vos;
    void updateInternals(void);
    uint8_t getValues(void);
};

#endif /* TMP006_HPP_ */
