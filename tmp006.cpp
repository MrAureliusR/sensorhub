/*
 * tmp006.cpp
 *
 *  Created on: Aug. 15, 2020
 *      Author: amr
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "tmp006.hpp"
#include "i2c.hpp"

tmp006::tmp006(i2c *givenBus) {
    const uint8_t config[] = { 0x02, 0xF8 };
    this->busAccess = givenBus;
    busAccess->changeAddress(tmp006Addr);
    busAccess->transact((uint8_t *)&config, 2, WRITE); // reset chip, set correct sample speed
}

void tmp006::getTemperature(double *temperature) {
    uint8_t errorCode = 0;
    errorCode = getValues();
    if(errorCode == SUCCESS) {
        updateInternals();
        *temperature = Tobj;
    } else {
        *temperature = -666.666;
    }
    return;
}

void tmp006::updateInternals(void) {
    double fVobj = 0, Tobj1 = 0, temp1 = 0, temp2 = 0;
    temp1 = a1 * (Tdie - Tref);
    temp2 = a2 * pow(Tdie - Tref, 2);
    S = S0 * (1 + temp1 + temp2);
    Vos = b0 + (b1*(Tdie - Tref)) + (b2*pow((Tdie - Tref), 2));
    fVobj = (Vobj - Vos) + c2*pow(Vobj - Vos, 2);
    Tobj1 = pow(Tdie, 4) + (fVobj / S);
    Tobj = pow(Tobj1, 0.25);
    return;
}

uint8_t tmp006::getValues(void) {
    const uint8_t reg[] = { 0x00, 0x01, 0x02 };
    int16_t tVobj, tTdie;
    uint8_t *result = (uint8_t *)malloc(sizeof(uint8_t) * 4);
    uint8_t errorCode = 0;
    busAccess->changeAddress(tmp006Addr);

    do {
        errorCode = busAccess->transact((uint8_t *)&reg[2], 1, WRITE);
        if(errorCode != SUCCESS) return errorCode;
        errorCode = busAccess->transact(result, 2, READ);
        if(errorCode != SUCCESS) return errorCode;
        __delay_cycles(14000);

    } while(!errorCode); // wait for data to be ready

    // now we are using result and errorcode for what they were meant for originally
    if(busAccess->transact((uint8_t *)&reg[0], 1, WRITE) != SUCCESS) { // set internal pointer to 0
        return errorCode;
    }
    if(busAccess->transact(result, 2, READ) != SUCCESS) { // read the two important registers
        return errorCode;
    }
    if(busAccess->transact((uint8_t *)&reg[1], 1, WRITE) != SUCCESS) { // set internal pointer to 0
        return errorCode;
    }
    if(busAccess->transact(result + 2, 2, READ) != SUCCESS) { // read the two important registers
        return errorCode;
    }

    // will this work? probably not!
    tVobj = (int16_t)((*result << 8) | *(result + 1));
    /*
     * We have to go through all this rigamarole because the Tdie value is a left-aligned signed 14-bit value
     * Yeah, I wouldn't have designed it that way either. Why couldn't they have just put zeros in bits 14 and 15?
     * Anyway, we need to get the value *without* the sign bit, shift it to the left by 2, then OR it with the sign bit.
     */
    tTdie = (int16_t)((*(result+2) << 8) | *(result + 3)); // & 0x7FFF; // ignore the sign bit for now
    tTdie >>= 2; // move two bits over
    tTdie |= (int16_t)(((*result+2) << 8) & 0x8000); // get the sign bit

    Vobj = (double)tVobj;
    Tdie = (double)tTdie;
    free(result);
    return SUCCESS;
}
